import sys

sys.path.append('./gen-py')
import cv2
import numpy as np
from PIL import Image
import os
from pic_similarity import PictureSimilarityService
from pic_similarity.ttypes import PictureSimilarityRequest, FileData
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TCompactProtocol

try:
    # make socket
    transport = TSocket.TSocket('localhost', 9090)

    # raw socket are very slow, so buffering is critical
    transport = TTransport.TBufferedTransport(transport)

    # wrap in a protocol
    protocol = TBinaryProtocol.TBinaryProtocol(transport)
    #protocol = TCompactProtocol.TCompactProtocol(transport)

    # create a client to use the protocal encoder
    client = PictureSimilarityService.Client(protocol)

    # Connect!
    transport.open()

    test_imgs = [img for img in os.listdir('./data') if img.startswith('test')]
    for test in test_imgs:
        buff = open(os.path.join('./data', test), 'rb').read()
        file_data = FileData(test, buff)
        req = PictureSimilarityRequest(seq_picture=file_data, k=3)
        result = client.predict(req)
        print('-'*20, test, '-'*20)
        print("top k score:", result.score)
        print("the top k picture's name:", result.filename)

    transport.close()

except Thrift.TException as ex:
    print("%s" % ex.message)