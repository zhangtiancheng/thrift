#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

sys.path.append('./gen-py')
from pic_similarity import PictureSimilarityService
from pic_similarity.ttypes import *
from thrift.server import TServer
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

import cv2
import numpy as np
import os
import os.path

__HOST = '127.0.0.1'
__PORT = 9090


def hist_similar(lhist, rhist, lpixal, rpixal):

    rscale = rpixal / lpixal
    rhist = rhist / rscale
    assert len(lhist) == len(rhist)
    likely = sum(1 - (0 if l == r else float(abs(l - r)) / max(l, r)) for l, r in zip(lhist, rhist)) / len(lhist)
    if likely == 1.0:
        return [1.0]
    return likely


def get_histGBR(path):

    img = cv2.imread(path)

    pixal = img.shape[0] * img.shape[1]
    total = np.array([0])
    for i in range(3):
        histSingle = cv2.calcHist([img], [i], None, [256], [0, 256])
        total = np.vstack((total, histSingle))
    return total, pixal


class PictureSimilarityHandler(PictureSimilarityService.Iface):
    def __init__(self):

        self.log = {}
        self.names = []
        self.hist = []
        root_dir = './data'
        files = os.listdir(root_dir)
        # print(files)
        for file in files:
            print(file)
            filepath = os.path.join(root_dir, file)
            temp_hist, temp_pixal = get_histGBR(filepath)
            # 1 = hist_similar(targetHist, testHist, targetPixal, testPixal)[0]
            self.names.append(file)
            self.hist.append((temp_hist, temp_pixal))
        self.length = len(self.names)

    def predict(self, req):

        pic_data = req.seq_picture
        buff = pic_data.buff
        k = req.k  # the param top k

        result_dict = {}

        # first, save the requested picture to the local disk of server
        save_path = './save_dir/save.jpg'
        with open('./save_dir/save.jpg', 'wb') as f:
            f.write(buff)
        targetHist, targetPixal = get_histGBR(save_path)
        for i in range(self.length):
            result_dict[self.names[i]] = hist_similar(targetHist, self.hist[i][0], targetPixal, self.hist[i][1])[0]

        sortedDict = sorted(result_dict.items(), key=lambda temp: temp[1], reverse=True)
        # print(sortedDict)
        scores = []
        names = []
        for i in range(k):
            names.append(sortedDict[i][0])
            scores.append(sortedDict[i][1])
        retn = PictureSimilarityResponse(1, scores, names)
        return retn


if __name__ == '__main__':
    handler = PictureSimilarityHandler()
    processor = PictureSimilarityService.Processor(handler)
    transport = TSocket.TServerSocket(host=__HOST, port=__PORT)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()

    server = TServer.TThreadPoolServer(processor, transport, tfactory, pfactory)

    print('Starting the server')
    server.serve()
    print('done')