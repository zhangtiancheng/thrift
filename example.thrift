namespace py pic_similarity

struct FileData{
    1: required string name,
    2: required string buff
}

struct PictureSimilarityRequest {
    1: required FileData seq_picture,
    2: required i32 k,

    3: optional string app = "default",
    4: optional bool debug = false
}

struct PictureSimilarityResponse {
    1: required i64 status,
    2: required list<double> score,
    3: required list<string> filename,

    4: optional string err_msg,
    5: optional string debug_msg
}


service PictureSimilarityService{
    PictureSimilarityResponse predict(1: PictureSimilarityRequest req)
}